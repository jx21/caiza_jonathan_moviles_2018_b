package jonathan.moviles.epn.examenmovilesbi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.os.CountDownTimer
import android.util.Log
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    internal lateinit var txtScoreGame: TextView;
    internal lateinit var botonIniciar: Button;
    internal lateinit var botonParar: Button;
    internal lateinit var txtRandom: TextView;

    internal var timeLeft = 10;
    internal var score = 0;
    internal var ramdom = -1;

    internal var gameStarted = false;
    internal lateinit var countDownTimer: CountDownTimer;
    internal val countDownInterval = 1000L;
    internal val initialCountDown = 10000L;

    internal val TAG = MainActivity::class.java.simpleName;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "onCreate called . Score is $score");
        txtScoreGame = findViewById<TextView>(R.id.txt_puntaje);
        botonIniciar = findViewById<Button>(R.id.btn_iniciar);
        botonParar = findViewById<Button>(R.id.btn_parar);
        txtRandom = findViewById<TextView>(R.id.txt_random);

        botonIniciar.setOnClickListener{ _ -> startGame()};
        botonParar.setOnClickListener{ _ -> incrementScore()};
        resetGame();
    }

    private fun resetGame(){

        timeLeft = 10;
        ramdom =-1;
        txtRandom.text = "?"
        gameStarted = false;

    }

    private fun incrementScore(){
        if(ramdom == timeLeft){
            score += 100;
        }else if (ramdom >= timeLeft-1 && ramdom <= timeLeft+1){
            score += 50;
        }else{
            score += 0;
        }
        txtScoreGame.text = getString(R.string.your_score, score.toString());

        if(!gameStarted){
            startGame();
        }
        countDownTimer.cancel();
        endGame();
    }

    private fun iniciarContador(){
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000;
            }

            override fun onFinish() {
                endGame();
            }
        }
    }

    private fun startGame(){

        iniciarContador();
        if(ramdom==-1){
            countDownTimer.start();
            ramdom = (0.. 10).shuffled().first();
            txtRandom.text= getString(R.string.random, ramdom.toString());
            gameStarted = true;
        }else{
            Toast.makeText(
                    this,
                    getString(R.string.error_message),
                    Toast.LENGTH_LONG).show();
        }
    }


    private fun endGame(){
        if(gameStarted) {
            Toast.makeText(
                    this,
                    getString(R.string.game_over_message, Integer.toString(score)),
                    Toast.LENGTH_LONG).show();
            countDownTimer.cancel();
            resetGame();
        }

    }

}
